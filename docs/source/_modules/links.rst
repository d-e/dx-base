.. _array interface: https://docs.scipy.org/doc/numpy-1.15.1/
   reference/arrays.interface.html

.. |PlanarShape| replace:: `PlanarShape
   <dx_utilities.geometry.planar.PlanarShape>`
.. |PlanarShape.new| replace:: `PlanarShape.new
   <dx_utilities.geometry.planar.PlanarShape.new>`
.. |UniformPlanarField| replace:: `UniformPlanarField
   <dx_utilities.fields.fields.UniformPlanarField>`
