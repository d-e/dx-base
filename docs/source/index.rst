Welcome to documentation of ``dx-base`` package
===============================================

Base packages and modules for structural design software
--------------------------------------------------------

Features
^^^^^^^^

* Structural elements in plane.
* Exceptions with error-code.
* Nodal forces.
* Structural materials.
* Material safety factors.

.. Usage
.. -----
.. .. toctree::
..    chapters/usage

.. toctree::
   :hidden:
   :glob:

   _modules/modules

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
